var express = require('express');
// representa la aplicación web
var app = express();
// crear una variable para coger el body enviado desde el navegador
var bodyParser = require('body-parser');
app.use(bodyParser.json());
// puerto en el que va a estar escuchando ntra app
var port = process.env.PORT||3000;
// comando para que empiece a funcionar y a escuchar
app.listen(port);
console.log("API escuchando en el puerto " + port);

// cada petición tiene una ruta y una función manejadora de las peticiones
app.get ("/apitechu/v1",
    // req=petición, res=respuesta
    function(req,res) {
        console.log("GET /apitechu/v1");
        //res.send("Respuesta desde apitechu");
        res.send ({"msg": "Hola desde Apitechu"});
    }
);

app.get("/apitechu/v1/users",
  function(req,res) {
      console.log("GET /apitechu/v1/users");
      /* opción 1 depprecated*/
      //"." indica dónde estoy ahora mismo en el path
      //res.sendFile('.usuarios.json'); // depprecated
      /* opción 2 */
      // en esta sentencia le digo desde dónde buscar con "root" y
      // dirname se refiere a dónde está el script que se está ejecutando
      res.sendFile('users_file.json', {root: __dirname});
      /* opción 3
      var users = require('./usuarios.json');
      res.send(users);*/
  }
);

app.post("/apitechu/v1/users",
  function(req,res) {
    console.log("POST /apitechu/v1/users");
    //console.log(req.headers);

    // Enviarlo en el headers
    /*
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.country);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name"  : req.headers.last_name,
      "country"    : req.headers.country
    };
    */

    // Enviarlo en body
    console.log("first_name is "+req.body.first_name);
    console.log("last_name is "+req.body.last_name);
    console.log("country is "+req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name"  : req.body.last_name,
      "country"    : req.body.country
    };
    var users = require('./users_file.json');
    //fución de array de javascript que añade un mimbro más al array
    users.push(newUser);
    writeUserDataToFile(users);

    console.log("Usuario guardado con éxito");
    res.send({"msg" : "Usuario guardado con éxito"});

    // lo utilizamos para persistir <guardar en bbdd>
    // lo llevamos a una función para reutizar en nuevo o delete
    /*
    var fs = require('fs');
    var jsonUserData = JSON.stringify(users);
    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
      function(err){
        if (err) {
          var msg = "Error al escribir el fichero usuarios"
          console.log(msg);
        }
        else {
          var msg = "Usuario persistido"
          console.log(msg);
        }
        res.send({"msg" : msg});
      }
    );
    */
  }

);

app.delete("/apitechu/v1/users/:id",
  function(req,res){
    console.log("DELETE /apitechu/v1/users");
    //console.log(req.params);
    console.log(req.params.id);

    var users = require('./usuarios.json');
    //empieza en la posición 10 (primer parametro)
    //y quita 1 (segundo parametro <longitud>)
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);

    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
);



app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
);

app.post("/apitechu/v1/login",
  function(req,res) {
    console.log("POST /apitechu/v1/login");

    // Enviarlo en body
    console.log("email is "+req.body.email);
    console.log("pass is "+req.body.pass);

    // recorrer el array a ver si existe
    var users = require('./users_file.json');
    var msg_res = {
        "mensaje" : "Login inorrecto"
    }

    for (user of users) {
      if ((user.email == req.body.email) && (user.pass == req.body.pass)) {
        user.logged = true;
        var msg_res = {
            "mensaje" : "Login correcto",
            "idUsuario" : user.id
        }
        writeUserDataToFile(users);
        break;
      }
    }
    res.send(msg_res);
  }

);

app.post("/apitechu/v1/logout",
  function(req,res) {
    console.log("POST /apitechu/v1/logout");

    // Enviarlo en body
    console.log("id is "+req.body.id);

    // recorrer el array a ver si existe
    var users = require('./users_file.json');
    var msg_res = {
        "mensaje" : "Logout incorrecto"
    }

    for (user of users) {
      if (user.id == req.body.id) {
        delete user.logged;
        var msg_res = {
            "mensaje" : "Logout correcto",
            "idUsuario" : user.id
        }
        writeUserDataToFile(users);
        break;
      }
    }
    res.send(msg_res);
  }

);

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users_file.json", jsonUserData, "utf8",
    function(err){
      if (err) {
        console.log(err);
      }
      else {
        console.log("Datos escritos en archivo");
      }
    }
  );
}
